import {Column, Entity, PrimaryColumn} from 'typeorm';
import {FragmentType} from '../enums/FragmentType';
import {Language} from '../enums/Language';

@Entity()
export class Fragment {

	@PrimaryColumn()
	public id: string;

	@Column()
	public lessonId: string;

	@Column({type: 'enum', enum: FragmentType})
	public type: FragmentType;

	@Column({type: 'enum', enum: Language})
	public language: Language;

	@Column()
	public audioId: string;

	@Column()
	public transcription: string;

	@Column({nullable: true})
	public glossId: number;

	constructor(id: string, lessonId: string, language: Language, type: FragmentType, audioId: string, transcription: string) {
		this.id = id;
		this.lessonId = lessonId;
		this.language = language;
		this.type = type;
		this.audioId = audioId;
		this.transcription = transcription;
	}
}
