import {Column, Entity, PrimaryColumn} from 'typeorm';

@Entity()
export class Lesson {

	@PrimaryColumn()
	public id: string;

	@Column()
	public ordinal: number;

	constructor(id: string, ordinal: number) {
		this.id = id;
		this.ordinal = ordinal;
	}
}
