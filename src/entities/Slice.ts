import {Column, Entity, PrimaryColumn} from 'typeorm';
import {FragmentType} from '../enums/FragmentType';
import {Language} from '../enums/Language';

@Entity()
export class Slice {

	@PrimaryColumn()
	public id: string;

	@Column()
	public fragmentId: string;

	@Column()
	public audioStart: number;

	@Column()
	public audioEnd: number;

	@Column()
	public textStart: number;

	@Column()
	public textEnd: number;

	constructor(id: string, fragmentId: string, audioStart: number, audioEnd: number, textStart: number, textEnd: number) {
		this.id = id;
		this.fragmentId = fragmentId;
		this.audioStart = audioStart;
		this.audioEnd = audioEnd;
		this.textStart = textStart;
		this.textEnd = textEnd;
	}
}
