import {Column, Entity, PrimaryColumn} from 'typeorm';

@Entity()
export class Audio {

	@PrimaryColumn()
	public id: string;

	@Column()
	public path: string;

	constructor(id: string, path: string) {
		this.id = id;
		this.path = path;
	}
}
