export enum FragmentType {
	Ordinal = 'Ordinal',
	Title = 'Title',
	Sentence = 'Sentence',
	Ubung = 'Ubung',
	Exercise = 'Exercise',
	Unknown = 'Unknown',
}
