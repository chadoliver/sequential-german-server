import express from 'express';
import {getManager} from 'typeorm';
import {Fragment} from '../../entities/Fragment';

import {Lesson} from '../../entities/Lesson';
import {Slice} from '../../entities/Slice';
import {FragmentType} from '../../enums/FragmentType';
import {Language} from '../../enums/Language';

export interface IComposedLesson {
	id: string;
	ordinal: number;
	title: string;
}

export interface IComposedFragment {
	id: string;
	language: Language;
	type: FragmentType;
	audioId: string;
	transcription: string;
	slices: IComposedSlice[];
}

export interface IComposedSlice {
	audioStart: number;
	audioEnd: number;
	textStart: number;
	textEnd: number;
}

export class PageHandler {

	public static async getIndexPageData(): Promise<IComposedLesson[]> {
		const entityManager = getManager();
		const lessons = await entityManager.find(Lesson);
		return Promise.all(lessons.map(async (lesson: Lesson): Promise<IComposedLesson> => {
			const title = await entityManager.findOne(Fragment, {
				where: {
					lessonId: lesson.id,
					type: FragmentType.Title,
				},
			});
			return {
				id: lesson.id,
				ordinal: lesson.ordinal,
				title: title.transcription,
			};
		}));
	}

	public static async getLessonPageData(req: express.Request): Promise<IComposedFragment[]> {
		const entityManager = getManager();
		const fragments = await entityManager.find(Fragment, {
			select: ['id', 'language', 'type', 'audioId', 'transcription'],
			where: {
				lessonId: req.params.id,
			},
		});
		return Promise.all(fragments.map(async (fragment: Fragment): Promise<IComposedFragment> => {
			return {
				id: fragment.id,
				language: fragment.language,
				type: fragment.type,
				audioId: fragment.audioId,
				transcription: fragment.transcription,
				slices: await entityManager.find(Slice, {
					select: ['audioStart', 'audioEnd', 'textStart', 'textEnd'],
					where: {
						fragmentId: fragment.id,
					},
				}),
			};
		}));
	}

	public static async getFragmentPageData(req: express.Request): Promise<IComposedFragment> {
		const entityManager = getManager();
		const fragment = await entityManager.findOne(Fragment, {
			select: ['id', 'language', 'type', 'audioId', 'transcription'],
			where: {
				id: req.params.id,
			},
		});
		return {
			id: fragment.id,
			language: fragment.language,
			type: fragment.type,
			audioId: fragment.audioId,
			transcription: fragment.transcription,
			slices: await entityManager.find(Slice, {
				select: ['audioStart', 'audioEnd', 'textStart', 'textEnd'],
				where: {
					fragmentId: fragment.id,
				},
			}),
		};
	}
}
