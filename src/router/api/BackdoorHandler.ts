import glob from 'glob-promise';
import _ from 'lodash';
import NodeID3 from 'node-id3';
import * as shortid from 'shortid';
import {getManager} from 'typeorm';

import {config} from '../../config';
import {Audio} from '../../entities/Audio';
import {Fragment} from '../../entities/Fragment';
import {Lesson} from '../../entities/Lesson';
import {Slice} from '../../entities/Slice';
import {FragmentType} from '../../enums/FragmentType';
import {Language} from '../../enums/Language';

export class BackdoorHandler {
	private static readonly directoryPattern = '/L*-German ASSIMIL';
	private static readonly filenamePattern = '/*.mp3';

	public static async load() {
		const entityManager = getManager();

		const dirPaths = await glob.promise(`${config.AUDIO_FILES_ROOT}${BackdoorHandler.directoryPattern}`);
		for (const dirPath of dirPaths) {
			const lessonNumber = BackdoorHandler.getLessonNumber(dirPath);
			const lesson = new Lesson(shortid.generate(), lessonNumber);
			await entityManager.save(lesson);

			const filePaths = await glob.promise(`${dirPath}${BackdoorHandler.filenamePattern}`);
			for (const filePath of filePaths) {
				const metadata = NodeID3.read(filePath);
				const titleParts = metadata.unsynchronisedLyrics.text.split(' : ');
				const fragmentType = BackdoorHandler.getFragmentTypeFromLabel(titleParts[0]);
				const transcription = titleParts[1];

				console.log(`${lessonNumber} - ${fragmentType} - ${transcription}`);

				const recording = new Audio(shortid.generate(), filePath);
				await entityManager.save(recording);

				const fragment = new Fragment(shortid.generate(), lesson.id, Language.German, fragmentType, recording.id, transcription);
				await entityManager.save(fragment);
			}
		}
		return 'done';
	}

	public static getLessonNumber(dirPath: string): number {
		const regexResult = dirPath.match(/L(\d+)-German ASSIMIL$/);
		if (regexResult.hasOwnProperty(1)) {
			const lessonNumber = Number(regexResult[1]);
			if (_.isFinite(lessonNumber)) {
				return lessonNumber;
			}
		}
		throw new Error(`Could not find a lesson number in the following path: ${dirPath}`);
	}

	private static getFragmentTypeFromLabel(tag: string): FragmentType {
		if (new RegExp(/^N\d{1,2}$/).test(tag)) {
			return FragmentType.Ordinal;
		} else if (new RegExp(/^S00-TITLE$/).test(tag)) {
			return FragmentType.Title;
		} else if (new RegExp(/^S\d\d$/).test(tag)) {
			return FragmentType.Sentence;
		} else if (new RegExp(/^T00-TRANSLATE$/).test(tag)) {
			return FragmentType.Ubung;
		} else if (new RegExp(/^T\d\d$/).test(tag)) {
			return FragmentType.Exercise;
		} else {
			return FragmentType.Unknown;
		}
	}

	public static async count() {
		let numSentences = 0;
		const filePaths = await glob.promise(BackdoorHandler.filenamePattern);
		for (const filePath of filePaths) {
			const titleParts = NodeID3.read(filePath).unsynchronisedLyrics.text.split(' : ');
			const transcription = titleParts[1];
			numSentences += 1;
		}
		return numSentences;
	}

	public static async wipe() {
		const entityManager = getManager();
		await entityManager.clear(Audio);
		await entityManager.clear(Fragment);
		await entityManager.clear(Lesson);
		await entityManager.clear(Slice);
		return 'done';
	}
}
