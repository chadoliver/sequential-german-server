import express from 'express';
import {getManager} from 'typeorm';

import {Lesson} from '../../entities/Lesson';

export class LessonHandler {

	public static async getAll(): Promise<Lesson[]> {
		const entityManager = getManager();
		return entityManager.find(Lesson);
	}

	public static async getById(req: express.Request): Promise<Lesson[]> {
		const entityManager = getManager();
		return entityManager.find(Lesson, {
			where: {
				id: req.params.id,
			},
		});
	}

	public static async getByFilter(req: express.Request): Promise<Lesson[]> {
		const entityManager = getManager();
		return entityManager.find(Lesson, {
			where: req.query,
		});
	}
}
