import express from 'express';
import {getManager} from 'typeorm';

import {Fragment} from '../../entities/Fragment';

export class FragmentHandler {

	public static async getAll(): Promise<Fragment[]> {
		const entityManager = getManager();
		return entityManager.find(Fragment);
	}

	public static async getById(req: express.Request): Promise<Fragment[]> {
		const entityManager = getManager();
		return entityManager.find(Fragment, {
			where: {
				id: req.params.id,
			},
		});
	}

	public static async getByFilter(req: express.Request): Promise<Fragment[]> {
		const entityManager = getManager();
		return entityManager.find(Fragment, {
			where: req.query,
		});
	}
}
