import express from 'express';
import {getManager} from 'typeorm';

import {Slice} from '../../entities/Slice';

export class SliceHandler {

	public static async getAll(): Promise<Slice[]> {
		const entityManager = getManager();
		return entityManager.find(Slice);
	}

	public static async getById(req: express.Request): Promise<Slice[]> {
		const entityManager = getManager();
		return entityManager.find(Slice, {
			where: {
				id: req.params.id,
			},
		});
	}

	public static async getByFilter(req: express.Request): Promise<Slice[]> {
		const entityManager = getManager();
		return entityManager.find(Slice, {
			where: req.query,
		});
	}

	public static async save(req: express.Request): Promise<void> {
		const entityManager = getManager();
		const {id, fragmentId, audioStart, audioEnd, textStart, textEnd} = req.body;
		const slice = new Slice(id, fragmentId, audioStart, audioEnd, textStart, textEnd);
		await entityManager.save(slice);
	}
}
