import express from 'express';
import _ from 'lodash';

import {BackdoorHandler} from './BackdoorHandler';
import {FragmentHandler} from './FragmentHandler';
import {LessonHandler} from './LessonHandler';
import {PageHandler} from './PageHandler';
import {SliceHandler} from './SliceHandler';

export type HandlerFunction<T> = (expressReq: express.Request, res: express.Response) => T;

function registerRoutes(router: express.Router) {
	router.get('/', handleRequest(() => 'hello'));

	router.get('/backdoor/load', handleRequest(BackdoorHandler.load));
	router.get('/backdoor/count', handleRequest(BackdoorHandler.count));
	router.get('/backdoor/wipe', handleRequest(BackdoorHandler.wipe));

	router.get('/lessons', handleRequest(LessonHandler.getAll));
	router.get('/lessons/filter', handleRequest(LessonHandler.getByFilter));
	router.get('/lesson/:id', handleRequest(LessonHandler.getById));

	router.get('/fragments', handleRequest(FragmentHandler.getAll));
	router.get('/fragments/filter', handleRequest(FragmentHandler.getByFilter));
	router.get('/fragment/:id', handleRequest(FragmentHandler.getById));

	router.get('/slices', handleRequest(SliceHandler.getAll));
	router.get('/slices/filter', handleRequest(SliceHandler.getByFilter));
	router.get('/slice/:id', handleRequest(SliceHandler.getById));
	router.post('/slice', handleRequest(SliceHandler.save));	// need to update this so that clients can specify an id

	router.get('/page/lessons', handleRequest(PageHandler.getIndexPageData));
	router.get('/page/lesson/:id', handleRequest(PageHandler.getLessonPageData));
	router.get('/page/fragment/:id', handleRequest(PageHandler.getFragmentPageData));
}

function handleRequest<T>(func: HandlerFunction<T>) {
	return async (req: express.Request, res: express.Response, next: express.NextFunction): Promise<void> => {
		try {
			const message: T = await func(req, res);
			const text = _.isString(message) ? message : JSON.stringify(message);
			sendResponse(req, res, text, 200);
			next();
		} catch (error) {
			next(error);
		}
	};
}

function gracefullyHandleErrors(err: Error, req: express.Request, res: express.Response, next: express.NextFunction): void  {
	if (res.headersSent) {
		next(err);
	} else {
		try {
			err = err || new Error('Server Error.');
			const messageString = err.toString();
			sendResponse(req, res, messageString, 500);
		} catch (secondError) {
			next(secondError);
		}
	}
}

function sendResponse(req: express.Request, res: express.Response, message: string, statusCode: number = 200): void {
	res.statusCode = res.statusCode || statusCode;
	res.set({
		'content-type': 'application/json; charset=utf-8',
		'content-length': _.size(message).toString(),
	});
	console.log('\nReceived request for:', req.path);
	console.log('Sending response:', message);
	res.send(message);
}

export const apiRouter = express.Router();

apiRouter.use(express.json());
registerRoutes(apiRouter);
apiRouter.use(gracefullyHandleErrors);
