import express from 'express';
import {AudioHandler} from './AudioHandler';

function registerRoutes(router: express.Router) {
	router.get('/:id', AudioHandler.getById);
}

export const audioRouter = express.Router();
registerRoutes(audioRouter);
