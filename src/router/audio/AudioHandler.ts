import express from 'express';
import {getManager} from 'typeorm';

import {Audio} from '../../entities/Audio';

export class AudioHandler {

	public static async getById(req: express.Request, res: express.Response, next: express.NextFunction): Promise<void> {
		const entityManager = getManager();
		const recording = await entityManager.findOne(Audio, {
			where: {
				id: req.params.id,
			},
		});

		res.sendFile(recording.path);
	}
}
